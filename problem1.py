# -*- coding: utf-8 -*-

def sum(limit):
    sum = 0;
    for i in range(1, limit):
        if (i % 3 == 0) or (i % 5 == 0):
            sum = sum + i
    return sum

def sum1(limit):
    x = (limit - 1) / 3
    y = (limit - 1) / 5
    z = (limit - 1) / 15
    return 3 * (x ** 2 + x) / 2 + 5 * (y ** 2 + y) / 2 - 15 * (z ** 2 + z) / 2


if __name__ == '__main__':
    limit = input("输入一个上限:")
    print sum1(limit)
